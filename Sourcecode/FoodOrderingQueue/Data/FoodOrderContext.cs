using FoodOrderingQueue.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace FoodOrderingQueue.Data
{
    public class FoodOrderContext : IdentityDbContext<NewsUser>
    {
        public DbSet<FoodMenu> FoodMenus { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<FoodOrder> FoodOrders { get; set; }
        public DbSet<Container> Containers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=FoodOrderingQueue.db");
        }

        public DbSet<FoodOrderingQueue.Models.Container> Container { get; set; }
    }
}
