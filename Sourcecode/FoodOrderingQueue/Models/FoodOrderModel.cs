using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace FoodOrderingQueue.Models {
    public class NewsUser : IdentityUser {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
    }

    public class Container {
        public int ContainerID { get; set; }
        public string ContainerName { get; set; }
    }

    public class Store {
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string StoreDescirbe { get; set; }

        public string NewsUserId { get; set; }
        public NewsUser postUser { get; set; }
    }

    public class FoodMenu {
        public int FoodMenuID { get; set; }

        public int StoreID { get; set; }
        public Store Stores { get; set; }

        public string FoodName { get; set; }
        public float Price { get; set; }
        public int RateTime { get; set; }

        public string NewsUserId { get; set; }
        public NewsUser postUser { get; set; }
    }

    public class FoodOrder {
        public int FoodOrderID { get; set; }

        public int StoreID { get; set; }
        public Store Stores { get; set; }

        public int FoodMenuID { get; set; }
        public FoodMenu FoodMenus { get; set; }

        public string FoodName { get; set; }
        public float Price { get; set; }
        public string FinshedTime { get; set; }

        public int ContainerID { get; set; }
        public Container Containers { get; set; }
        
        public string Describe { get; set; }

        public string NewsUserId { get; set; }
        public NewsUser postUser { get; set; }
    }
    

}