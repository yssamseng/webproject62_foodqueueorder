using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FoodOrderingQueue {
    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.Configure<CookiePolicyOptions> (options => {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<FoodOrderContext> ();
            services.AddDefaultIdentity<NewsUser> ()
                .AddEntityFrameworkStores<FoodOrderContext> ();
            services.AddMvc ().SetCompatibilityVersion (CompatibilityVersion.Version_2_1)
                .AddRazorPagesOptions (options => {
                    options.Conventions.AuthorizeFolder ("/StoreAdmin");
                    options.Conventions.AuthorizeFolder ("/FoodMenuAdmin");
                    options.Conventions.AuthorizeFolder ("/Order");
                    options.Conventions.AuthorizeFolder ("/FoodOrderAdmin");
                    options.Conventions.AuthorizeFolder ("/ContainerAdmin");
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IHostingEnvironment env) {
            if (env.IsDevelopment ()) {
                app.UseDeveloperExceptionPage ();
            } else {
                app.UseExceptionHandler ("/Error");
            }

            app.UseStaticFiles ();
            app.UseCookiePolicy ();
            app.UseAuthentication ();

            app.UseMvc ();
        }
    }
}