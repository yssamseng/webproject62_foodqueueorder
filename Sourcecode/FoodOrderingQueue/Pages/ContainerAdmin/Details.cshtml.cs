using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.ContainerAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public DetailsModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public Container Container { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Container = await _context.Container.FirstOrDefaultAsync(m => m.ContainerID == id);

            if (Container == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
