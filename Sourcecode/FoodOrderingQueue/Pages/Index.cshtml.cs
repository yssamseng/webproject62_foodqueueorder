﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FoodOrderingQueue.Pages {
    public class IndexModel : PageModel {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public IndexModel (FoodOrderingQueue.Data.FoodOrderContext context) {
            _context = context;
        }

        public IList<FoodMenu> FoodMenu { get; set; }
        public IList<Store> Store { get; set; }
        public IList<FoodOrder> FoodOrder { get; set; }

        public async Task OnGetAsync () {
            FoodMenu = await _context.FoodMenus
                .Include (f => f.postUser)
                .Include (f => f.Stores).ToListAsync ();

            Store = await _context.Stores.ToListAsync ();
            
            FoodOrder = await _context.FoodOrders
                .Include (f => f.Containers)
                .Include (f => f.Stores)
                .Include (f => f.postUser).ToListAsync ();
        }
    }
}