using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodMenuAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public DetailsModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public FoodMenu FoodMenu { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodMenu = await _context.FoodMenus
                .Include(f => f.Stores).FirstOrDefaultAsync(m => m.FoodMenuID == id);

            if (FoodMenu == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
