using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;


namespace FoodOrderingQueue.Pages.FoodMenuAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public CreateModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IList<Store> Store { get;set; }

        public async Task<IActionResult> OnGetAsync()
        {
            ViewData["StoreID"] = new SelectList(_context.Stores, "StoreID", "StoreName");
            Store = await _context.Stores
                .Include (f => f.postUser).ToListAsync();
            return Page();
        }

        [BindProperty]
        public FoodMenu FoodMenu { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.FoodMenus.Add(FoodMenu);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}