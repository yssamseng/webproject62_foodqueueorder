using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodMenuAdmin
{
    public class IndexModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public IndexModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IList<FoodMenu> FoodMenu { get;set; }

        public async Task OnGetAsync()
        {
            FoodMenu = await _context.FoodMenus
                .Include(p => p.postUser)
                .Include(f => f.Stores).ToListAsync();
        }
    }
}
