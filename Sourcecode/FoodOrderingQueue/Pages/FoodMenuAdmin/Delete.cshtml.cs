using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodMenuAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public DeleteModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FoodMenu FoodMenu { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodMenu = await _context.FoodMenus
                .Include(f => f.Stores).FirstOrDefaultAsync(m => m.FoodMenuID == id);

            if (FoodMenu == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodMenu = await _context.FoodMenus.FindAsync(id);

            if (FoodMenu != null)
            {
                _context.FoodMenus.Remove(FoodMenu);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
