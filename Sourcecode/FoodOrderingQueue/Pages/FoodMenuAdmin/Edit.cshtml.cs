using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodMenuAdmin
{
    public class EditModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public EditModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FoodMenu FoodMenu { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodMenu = await _context.FoodMenus
                .Include(f => f.Stores).FirstOrDefaultAsync(m => m.FoodMenuID == id);

            if (FoodMenu == null)
            {
                return NotFound();
            }
           ViewData["StoreID"] = new SelectList(_context.Stores, "StoreID", "StoreID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(FoodMenu).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodMenuExists(FoodMenu.FoodMenuID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool FoodMenuExists(int id)
        {
            return _context.FoodMenus.Any(e => e.FoodMenuID == id);
        }
    }
}
