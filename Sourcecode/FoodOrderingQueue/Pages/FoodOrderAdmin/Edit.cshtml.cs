using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodOrderAdmin
{
    public class EditModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public EditModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FoodOrder FoodOrder { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodOrder = await _context.FoodOrders
                .Include(f => f.Containers)
                .Include(f => f.FoodMenus)
                .Include(f => f.Stores).FirstOrDefaultAsync(m => m.FoodOrderID == id);

            if (FoodOrder == null)
            {
                return NotFound();
            }
           ViewData["ContainerID"] = new SelectList(_context.Container, "ContainerID", "ContainerID");
           ViewData["FoodMenuID"] = new SelectList(_context.FoodMenus, "FoodMenuID", "FoodMenuID");
           ViewData["StoreID"] = new SelectList(_context.Stores, "StoreID", "StoreID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(FoodOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodOrderExists(FoodOrder.FoodOrderID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool FoodOrderExists(int id)
        {
            return _context.FoodOrders.Any(e => e.FoodOrderID == id);
        }
    }
}
