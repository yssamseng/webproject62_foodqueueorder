using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodOrderAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public DetailsModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public FoodOrder FoodOrder { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FoodOrder = await _context.FoodOrders
                .Include(f => f.Containers)
                .Include(f => f.FoodMenus)
                .Include(f => f.Stores).FirstOrDefaultAsync(m => m.FoodOrderID == id);

            if (FoodOrder == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
