using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodOrderAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public CreateModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ContainerID"] = new SelectList(_context.Container, "ContainerID", "ContainerID");
        ViewData["FoodMenuID"] = new SelectList(_context.FoodMenus, "FoodMenuID", "FoodMenuID");
        ViewData["StoreID"] = new SelectList(_context.Stores, "StoreID", "StoreID");
            return Page();
        }

        [BindProperty]
        public FoodOrder FoodOrder { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.FoodOrders.Add(FoodOrder);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}