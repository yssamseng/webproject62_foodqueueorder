using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.FoodOrderAdmin
{
    public class IndexModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public IndexModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IList<FoodOrder> FoodOrder { get;set; }

        public async Task OnGetAsync()
        {
            FoodOrder = await _context.FoodOrders
                .Include(f => f.Containers)
                .Include(f => f.FoodMenus)
                .Include(p => p.postUser)
                .Include(f => f.Stores).ToListAsync();
        }
    }
}
