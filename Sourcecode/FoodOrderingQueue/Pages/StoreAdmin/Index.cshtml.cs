using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.StoreAdmin
{
    public class IndexModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public IndexModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IList<Store> Store { get;set; }

        public async Task OnGetAsync()
        {
            Store = await _context.Stores
                .Include(p => p.postUser).ToListAsync();
        }
    }
}
