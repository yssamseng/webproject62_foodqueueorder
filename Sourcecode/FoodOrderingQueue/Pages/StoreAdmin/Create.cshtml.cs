using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FoodOrderingQueue.Data;
using FoodOrderingQueue.Models;

namespace FoodOrderingQueue.Pages.StoreAdmin
{
    public class CreateModel : PageModel
    {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public CreateModel(FoodOrderingQueue.Data.FoodOrderContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Store Store { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Stores.Add(Store);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}