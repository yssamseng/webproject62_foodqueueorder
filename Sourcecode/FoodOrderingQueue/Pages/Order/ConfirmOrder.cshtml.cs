using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodOrderingQueue.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FoodOrderingQueue.Pages {
    public class ConfirmOrderModel : PageModel {
        private readonly FoodOrderingQueue.Data.FoodOrderContext _context;

        public ConfirmOrderModel (FoodOrderingQueue.Data.FoodOrderContext context) {
            _context = context;
        }

        public IList<FoodMenu> FoodMenus { get;set; }
        public IList<FoodOrder> FoodOrders { get;set; }
        public IList<Store> Store { get;set; }
        
        [BindProperty]
        public FoodOrder FoodOrder { get; set; }
        public FoodMenu FoodMenu { get; set; }
        public int storeId;
        public int foodId;
        public string foodName;
        public float foodPrice;
        public int rateTime;

        public async Task<IActionResult> OnGetAsync () {
            ViewData["ContainerID"] = new SelectList(_context.Containers, "ContainerID", "ContainerName");
            ViewData["NewsUserId"] = new SelectList(_context.Users, "Id", "Id");
            ViewData["StoreId"] = new SelectList(_context.Stores, "StoreId", "StoreName");
            storeId = int.Parse (Request.Query["storeId"]);
            foodId = int.Parse (Request.Query["foodId"]);
            foodName = Request.Query["foodName"];
            foodPrice = float.Parse (Request.Query["price"]);
            rateTime = int.Parse (Request.Query["rateTime"]);

            FoodMenus = await _context.FoodMenus
                .Include(p => p.postUser)
                .Include(f => f.Stores).ToListAsync();
            Store = await _context.Stores
                .Include(p => p.postUser).ToListAsync();
            FoodOrders = await _context.FoodOrders.ToListAsync();
            return Page ();
        }

        public async Task<IActionResult> OnPostAsync () {
            if (!ModelState.IsValid) {
                return Page ();
            }

            _context.FoodOrders.Add (FoodOrder);
            await _context.SaveChangesAsync ();

            return RedirectToPage ("./Index");
        }
    }
}